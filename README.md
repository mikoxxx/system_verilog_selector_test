# system_verilog_selector_test

#### 介绍
使用System-Verilog，开发的一个自动测试程序。
功能：实现选择通道输出功能。如果超过设定的地址，则从B通道输出，否则从A通道输出。输出地址与数据，和输入一致.
测试：循环20次，每次输入随机产生的地址和数据


#### 软件架构
1.rtl: 代码存放位置
    a.design.sv: 实现一个根据输入地址，选择通道输出功能。如果超过设定的地址，则从B通道输出，否则从A通道输出。输出地址与数据，和输入一致
    b.trsaction: 创建一个数据类，里面包含设计模块需要用到的相关数据。
    c.generator：创建测试的过程，此程序是自动循环20次，每次给随机地址和数据。
    d.driver   : 驱动测试过程
    e.monitor  : 获取测试结果
    f.scoreboard:将获取的测试结果与理想请情况的输出进行对比，输出对应的测试日志
    g.environment: 将generator  driver    monitor  scoreboard，整合到一个测试环境中
    h.test: 例化environment，生成一个测试平台    
    i.interface: 用于连接测试平台和激励文件的桥梁
    j.tb: 例化测试平台和接口。产生对应的时钟，输出到接口。启动测试平台，以及生成对应的测试日志或者vcd文件

2.sim：modelsim仿真工程
    
直接打开工程文件就能用。

写代码时最大的坑在于，文件的重复引用。在modelsim上编译不通过。
    
建议：
1.在需要被引用的文件中加入一下代码：
`ifndef DRIVER_SV
`define DRIVER_SV

`endif

前两行放在最开头，`endif放在最后面，“DRIVER_SV”--每个文件都需要用不同的名字


代码参考：https://www.chipverify.com/systemverilog/systemverilog-testbench-example-2




