`ifndef DRIVER_SV
`define DRIVER_SV
`include "transaction.sv"
class driver;
    virtual switch_if vif;  // 虚拟接口
    event drv_done;
    mailbox drv_mbx;

    task run();
        $display("T=%0t [Driver] staring ...", $time);
        @(posedge vif.clk);
        forever begin
            switch_item item;

            $display("T=%0t [Driver] waiting for item ...", $time);
            drv_mbx.get(item);
            item.print("Driver");
            // 将相关数据放入虚拟接口
            vif.vld     <= 1;
            vif.addr    <= item.addr;
            vif.data    <= item.data;

            // 等待下一个上升沿，将vld拉低
            @(posedge vif.clk);
            vif.vld     <= 0;
            // 等待一次驱动时间结束
            ->drv_done;
        end
    endtask
endclass //className
`endif