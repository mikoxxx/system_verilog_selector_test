// 搭建测试环境
`ifndef ENVIRONMENT_SV
`define ENVIRONMENT_SV


`include "generator.sv"
`include "driver.sv"
`include "monitor.sv"
`include "scoreboard.sv"

class env;
    driver                  d0;
    monitor                 m0;
    generator               g0;
    scoreboard              s0;

    mailbox         drv_mbx;
    mailbox         scb_mbx;
    event           drv_done;

    virtual         switch_if vif;

    // 例化对应的模块 和mailbox，以及事件  输出到对应的模块
    function new();
        d0 = new;  
        m0 = new; 
        g0 = new; 
        s0 = new;

        drv_mbx = new();
        scb_mbx = new();

        d0.drv_mbx = drv_mbx;
        g0.drv_mbx = drv_mbx;

        m0.scb_mbx = scb_mbx;
        s0.scb_mbx = scb_mbx;

        d0.drv_done = drv_done;
        g0.drv_done = drv_done;
    endfunction

    // 将接口输出到driver monitor
    virtual task run();
        d0.vif = vif;
        m0.vif = vif;
        // fork-join_any中任意一个进程执行完毕，就可以继续下面进程的执行。
        // 当然，fork-join_any中没有执行完的语句会继续执行
        // 相当于并行操作
        fork
            d0.run();
            m0.run();
            g0.run();
            s0.run();
        join_any
    endtask
endclass
`endif