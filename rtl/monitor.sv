`ifndef MONITOR_SV
`define MONITOR_SV
`include "transaction.sv"
class monitor;
    virtual switch_if vif;  // 虚拟接口
    mailbox scb_mbx;
    semaphore   sema4;

    function new ();
        sema4 = new(1);
    endfunction

    task run();
        $display("T=%0t [Monitor] staring ...", $time);

        // 这里创建了两个线程
        // fork-join将会并行的执行块中的进程，当fork-join中的所有进程都执行完了，才会继续执行下面的语句。
        fork
            sample_port("Thread0");
            sample_port("Thread1");
        join 
    endtask

    task sample_port(string tag="");
        forever begin
             // 这里是时序电路的软件写法
            @(posedge vif.clk);
            // 当复位为高（低有效），且vld信号拉高，开始运行线程
            if (vif.rstn & vif.vld) begin
                switch_item item = new; // 实例化一个数据类
                sema4.get();
                item.addr = vif.addr;   // 地址输入
                item.data = vif.data;   // 数据输入
                $display("T=%0t [Monitor] %s First part over", $time, tag);

                // 下一个时钟上升沿，开始输出数据
                @(posedge vif.clk);
                sema4.put();
                // 将接口转发的输出数据，放入实例化的数据类
                item.addr_a = vif.addr_a;
                item.data_a = vif.data_a;
                item.addr_b = vif.addr_b;
                item.data_b = vif.data_b;
                $display("T=%0t [Monitor] %s Second part over", $time, tag);

                scb_mbx.put(item); // 将数据放入scoreboard的mailbox，以便观察和对比
                item.print({"Monitor_", tag});
            end
        end
    endtask
endclass
`endif