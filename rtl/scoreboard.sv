// 将测试结果与理想结果进行对比
//`include "transaction.sv"
`ifndef SCOREBOARD_SV
`define SCOREBOARD_SV
`include "transaction.sv"
class scoreboard;
    mailbox scb_mbx;
    
    task run();
        forever begin
            switch_item item;
            scb_mbx.get(item);  // 从monitor接收到的数据，在这里取出来
            // 下面是打印对比输出结果的日志
            if (item.addr inside {[0:'h3f]}) begin
                if (item.addr_a != item.addr | item.data_a != item.data)
                    $display("T=%0t [Scoreboard] ERROR! Mismatch addr=0x%0h data=0x%0h addr_a=0x%0h data_a=0x%0h", $time, item.addr, item.data, item.addr_a, item.data_a);
                else
                    $display("T=%0t [Scoreboard] PASS! Mismatch addr=0x%0h data=0x%0h addr_a=0x%0h data_a=0x%0h", $time, item.addr, item.data, item.addr_a, item.data_a);
            end
            else begin
                if (item.addr_b != item.addr | item.data_b != item.data)
                    $display("T=%0t [Scoreboard] ERROR! Mismatch addr=0x%0h data=0x%0h addr_b=0x%0h data_b=0x%0h", $time, item.addr, item.data, item.addr_b, item.data_b);
                else
                    $display("T=%0t [Scoreboard] PASS! Mismatch addr=0x%0h data=0x%0h addr_b=0x%0h data_b=0x%0h", $time, item.addr, item.data, item.addr_b, item.data_b);
            end
        end
    endtask
endclass 
`endif