`include "test.sv"
`include "interface.sv"
module tb;
    reg clk;

    always #10          clk = ~clk; // 产生时钟

    switch_if           _if (clk); // 将时钟输入到接口
    // 例化设计模块
    switch  u0(
        .clk             (clk),
        .rstn            (_if.rstn),
        .vld             (_if.vld),
        .addr            (_if.addr),
        .data            (_if.data),
        .addr_a          (_if.addr_a),
        .data_a          (_if.data_a),
        .addr_b          (_if.addr_b),
        .data_b          (_if.data_b)  
    );

    test t0;

    // 启动测试平台
    initial begin
        {clk, _if.rstn} <= 0;

        #20
        _if.rstn <= 1;
        t0 = new;
        t0.e0.vif = _if;
        t0.run();

        #50 $finish;
    end

    initial begin
        $dumpvars;
        $dumpfile("dump.vcd"); // 生成一个vcd文件
    end
endmodule