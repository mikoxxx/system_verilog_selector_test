// 搭建测试平台
`include "environment.sv"
class test;
    env e0; // 引用环境类

    // 例化环境
    function new();
        e0 = new;
    endfunction

    // 运行环境的任务
    task run();
        e0.run();
    endtask
endclass