`ifndef TRANSACTION_SV
`define TRANSACTION_SV
// transation的目的是构建DUT所需要的数据类
class switch_item;
    rand bit [7:0]      addr;   // 产生随机8位地址
    rand bit [15:0]     data;   // 产生随机16位数据

    bit [7:0]      addr_a;
    bit [15:0]     data_a;

    bit [7:0]      addr_b;
    bit [15:0]     data_b;


    function void print (string tag="");
        $display("T=%0t %s addr=0x%0h data=0x%0h addr_a=0x%0h data_a=0x%h addr_b=0x%0h data_b=0x%0h",
            $time, tag, addr, data, addr_a, data_a, addr_b, data_b); 
    endfunction
endclass
`endif